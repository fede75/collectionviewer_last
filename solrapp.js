var app = angular.module('solrapp', ['angularUtils.directives.dirPagination']);
    angular.module('solrapp')

      .directive('searchResults', function () { 
        return {
            scope: {
                solrUrl: '@',
                displayField: '@',
                query: '@',
                results: '&'
            },
			
			
					
            restrict: 'E',
            controller: function ($scope, $http) {
				$scope.currentPage = 1;
                $scope.pageSize = 10;
                $scope.$watch('query', function () {
										
                    $http({
                        method: 'GET',
                        url: $scope.solrUrl,
                        params: {
                            //'json.wrf': 'JSON_CALLBACK',
                            'q': $scope.query,
                            'fl': $scope.displayField,
							'wt':'json',
							'fq':'ispartof:"'+$scope.myFq+'"', 
						
							'qf':'pid',
							//'facet':'true',
                            //'defType':'edismax',
							'rows':'200'/*$scope.elementiperpagina*/,
						    'start':'0'
							
																				
                        }
                    })
                        
						
						 .success(function (data) {
                         var docs = data.response;
						 console.log(data.response);
                         $scope.results.docs = data.response.docs;
						 console.log(data.response.docs);
					
					
 				         
			
										
						

                    })
					
					
					
					
					
					
					.error(function (e) {console.log('Errore: '+e);});
                });
				
				
						
				
				
				
				
				
            },
			
			
				
			
            template: '<input ng-model="query" name="Search"></input>' +
			    '<select ng-model="myFq"> ' +
				'	<option value="o:152073">Pergamene</option> '+
				'	<option value="o:268280">La Gran Carta del Padovano di G. A. Rizzi Zannoni (1780)</option> '+
				'	<option value="o:109550">Biblioteca Elettronica di Linguistica e Filologia</option> '+
				'	<option value="o:74181">Il Gabinetto di Fisica di Giovanni Poleni</option> '+
				'	<option value="o:62504">Tavole museo botanicp</option> '+
				'</select>'+									
                '<h2>Risultati della ricerca {{query}}</h2>' +
               
			   '<div dir-paginate="doc in results.docs | itemsPerPage: pageSize" current-page="currentPage">' +
			   '<table>'+
               
				'  <p>{{doc["pid"]}}</p>' +
				'  <p>{{doc["owner"]}}</p>' +
				'  <p>{{doc["dc_description"]}}</p>' +
				'<p><img src=\'https://phaidradev.cab.unipd.it/preview/{{doc["pid"]}}/ImageManipulator/boxImage/480/png\'</p>'+
				'</table></div>'+

					
				'<dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="dirPagination.tpl.html"></dir-pagination-controls>'
				 


							
			    
        };
    });
	
	
	
	

	
	
	
	
	