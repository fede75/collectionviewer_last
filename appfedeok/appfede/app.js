

(function(){
	
	
	 
	
	
	
	
define(function (require) {
	
  'use strict';


  require('angular');
  require('ngAnimate');
  require('ngTouch');
  require('ngSanitize');
  require('angularTranslate');
  require('angularTranslateLoaderStaticFiles');
  require('promiseTracker');
  require('dirPagination');

  
  var app = angular.module('collectionApp', [
    'ngAnimate',
    'ngTouch',
    'ngSanitize',
    'angularUtils.directives.dirPagination',
    'pascalprecht.translate',
    'ajoslin.promise-tracker'
  ]);

 
  app.init = function () {
    angular.bootstrap(document.getElementById('collectionApp'), ['collectionApp']);
  };
    
  
  app
     
    .config(function($sceDelegateProvider) {
      $sceDelegateProvider.resourceUrlWhitelist(['self', '**']);
    })

    
    .config(['$translateProvider', function($translateProvider) {

      $translateProvider.useSanitizeValueStrategy('sanitizeParameters');

     
      $translateProvider.useStaticFilesLoader({
        prefix: 'https://phaidra-static.cab.unipd.it/collectionviewer/app/languages/',
        suffix: '.json'
      });

     	
      $translateProvider.preferredLanguage('it');
    }])

    .config(function($httpProvider) {
      
      $httpProvider.useApplyAsync(true);
    })

   
    .directive("collectionDirective", function() {
      return {
          templateUrl :"template.html"
      };
    })

    
    .directive('fadeIn', function(){
        return {
            restrict: 'AE',
            link: function($scope, $element, attrs) {
                $element.addClass("ng-hide-remove");                                 
                $element.on('load', function() {
                    $element.addClass("ng-hide-add"); 
                    $element.removeClass("loading"); 
                });
            }
        };
    })

   
    
    .factory('collectionService', ['$http', function($http) {  
	
     

     var service = {
          getCollection: getCollection
      };
      return service;
	  
      function getCollection(pid, from, limit, fields, instance) {
		  
		  
		  
			 	
			  	  
     return $http({
                          method  : 'GET',
                          url     : instance + '/object/'+pid+'/related',
                          params  : {
                                     'from': from,
                                     'limit': limit,  
                                     'relation': 'info:fedora/fedora-system:def/relations-external#hasCollectionMember',
                                     'fields': fields
                                    },
                          cache : true,
                          timeout: 60000
                       });
      }
    }])
	
	
	

    
    .factory('metadataService', ['$http', function($http) {      
      var service = {
          getMetadata: getMetadata
      };
      return service;
      function getMetadata(pid, instance)    {
		  
		  
		
  
		  
		  
		  
          return $http({
                       method : 'GET',
                       url    : instance + '/object/' + pid + '/dc',
                       cache  : true,
                       timeout: 60000
                    });
      }
    }])
	
	  
    .factory('metadataMuseoService', ['$http', function($http) {
      var service = {
          getMetadata: getMetadata
      };
      return service;
      function getMetadata(pid, instance)    {
          return $http({
                       method : 'GET',
                       url    : instance + '/render_metadata_museo/' + pid,
                       cache  : true,
                       timeout: 60000
                    });
      }
    }])

 
    .filter('searchTitleDescFilter', function() {
      return function(items,query)  {
        if (query === '')  {
         return items;
        }
        else  {
          var filtered = [];
          var letterMatch = new RegExp(query, 'i');
          for (var i = 0; i < items.length; i++) {
              var item = items[i];
              if (letterMatch.test(item.title) || letterMatch.test(item.description) || letterMatch.test(item.pid)) {
                filtered.push(item);
              }
          }
          return filtered;
       }	
      };
    })

    
    .filter('parseUrlFilter', function() {
     
      var replacePattern = /({link}|{\/link})/gi;
      var urlPattern = /(http?|ftp):\/\/[\w-]+(\.[\w-]+)+([\w-.,@_?^=%&;:|!\/~+#()]*[\w-@_?^=%&;|!\/~+#()])?/gi;
      return function (text) {
        var text = String(text).trim(); 
        return (text.length > 0) ? text.replace(replacePattern, ' ').replace(urlPattern, '<a target="_blank" href="$&">$&</a>') : null;
      };
    })

   
    .filter('nl2br', function() {
      return function(text){
        var text = String(text).trim(); 
        return (text.length > 0) ? text.replace(/[\r\n]/g, '<br />') : null;
      };
    })

    
    .filter('highlight', function() {
      return function(text, phrase) {
        var text = String(text).trim();
        return (phrase !== '') ? text.replace(new RegExp('('+phrase+')', 'gi'),
          '<span class="highlighted">$1</span>') : text;
      };
    })

    
    .controller('CollectionController', ['$scope', '$location', '$animate', '$translate', '$interval', 'promiseTracker', 'collectionService', 'metadataService', 'metadataMuseoService', function($scope, $location, $animate, $translate, $interval, promiseTracker, collectionService, metadataService, metadataMuseoService) { 
    $scope.phaidraInstance = 'https://phaidra.cab.unipd.it';
    $scope.apiInstance = 'https://phaidra.cab.unipd.it/api';
    $scope.fedoraInstance = 'https://fedoradev.cab.unipd.it';
    $scope.loadingCollectionTracker = promiseTracker();
    $scope.pidError = 0;
    $scope.objects = [];
    $scope.collectionAlerts = [];
    $scope.collectionError = {status: ''};
    $scope.collectionMetadata = [];
    $scope.collectionMetadataAlerts = [];
    $scope.collectionMetadataError = {status: ''};
    $scope.collectionTitle = '';    
    $scope.itemMetadata = [];
    $scope.itemMetadataAlerts = [];
    $scope.itemMetadataError = {status:''};
    $scope.query = '';
    $scope.maxSize = 7;
    $scope.autoHide = true;
    $scope.currentPage = 1;
    $scope.showMeta = 0;
    $scope.resourceURL = '';
    $scope.hovering = false;
    $scope.year = new Date().getFullYear();
    $scope.appLanguage = 'ita';
    $scope.appLanguageFallback = 'eng';
    $scope.maxCollectionSize = 1000;
    $scope.largeCollectionFlag = 0;
    $scope.hasMUSEO = 0;
    $scope.MUSEOmetadata = '';
    $scope.detail = detail;
	$scope.fetchCollection=fetchCollection;

    if ($scope.collection.pid !== "" && $scope.collection.pid.match(/^o\:[0-9]+$/)) {
      $scope.onlyPID = $scope.collection.pid.replace("o:","");
      getCollectionDetail($scope.collection.pid);
      fetchCollection();
    }
    else  {
      
      $scope.pidError = 1;
    }

    
    $scope.prev = prev;
    function prev(posCurrent) {
      if ((posCurrent-1) >= 0) {
        $scope.detail($scope.objects[posCurrent-1]);
      }
    }

    
    $scope.next = next;
    function next(posCurrent) {
      if ((posCurrent+1) <= ($scope.objects.length-1)) {
        $scope.detail($scope.objects[posCurrent+1]);
      }
    }

    
    function processMetadata(itemMetadata) {
      
      var metadataObj = {
        'rights': {text:'', pos:8, sep:' - '},
        'coverage': {text:'', pos:5, sep:' - '},
        'language': {text:'', pos:7, sep:', '},
        'date': {text:'', pos:-1, sep:' - '},
        'subject': {text:'', pos:6, sep:' - '},
        'description': {text:'', pos:4, sep:' - '},
        'identifier': {text:'', pos:9, sep:' - '},
        'format': {text:'', pos:-1, sep:' - '},
        'type': {text:'', pos:-1, sep:' - '},
        'title': {text:'', pos:0, sep:' - '},
        'contributor': {text:'', pos:2, sep:' - '},
        'publisher': {text:'', pos:3, sep:' - '},
        'creator': {text:'', pos:1, sep:' - '},
        'source': {text:'', pos:-1, sep:' - '},
        'relation': {text:'', pos:-1, sep:' - '},
	'preview_page':{text:'', pos:-1, sep:' - '}
      };	
      	
      
      $scope.metadataLanguage = itemMetadata.filter(function (el) {
        return el.xmlname === 'language';
      })[0].ui_value;


      var itemMetadataLength = itemMetadata.length;
      for (var i = 0; i < itemMetadataLength; i++) {
	var item = itemMetadata[i];
	
	if (item.xmlname === 'subject') {
	  
	  if (item.ui_value.match(/^info\:eu-repo.*/i)) {	
	    continue;
	  }
	  if (item.attributes && item.attributes[0].xmlname === 'xml:lang') {
	    
	    if (item.ui_value.match(/^dewey.*/i) && item.attributes[0].ui_value === $scope.appLanguageFallback) {
	      continue;
	    }
	    
	    if (item.attributes[0].ui_value !== $scope.appLanguage && item.attributes[0].ui_value !== $scope.appLanguageFallback) {
  	      continue;
	    }	  
	  }
	}
	// TODO handle multilanguage 'ita' and 'eng'
	if (metadataObj[item.xmlname].text) {
	  metadataObj[item.xmlname].text += metadataObj[item.xmlname].sep + $translate.instant(item.ui_value);
	}
	else {
	  metadataObj[item.xmlname].text += $translate.instant(item.ui_value);
	}
      }
      return metadataObj;
    }
  
    function processObjects(objects)  {
      
      objects = objects.sort(function(obj1, obj2) {
	if (obj1.pid < obj2.pid){
	  return -1;
	}else if (obj1.pid > obj2.pid){
	  return 1;
	}else{
	  return 0;
	}
      });
      angular.forEach(objects, function(item, index)  {
	// Set thumbnail URL                    
	item.thumbnailURL = $scope.phaidraInstance + '/preview/' + item.pid + '///120';
	// Set item position in collection
	item.pos = index;	     
      });
      return objects;
    }

    // Fetch collection objects
    function fetchCollection() {      
      var from = 1;
      var fields = ['dc.description'];
      var promise = collectionService.getCollection($scope.collection.pid, from, $scope.maxCollectionSize, fields, $scope.apiInstance);
      $scope.loadingCollectionTracker.addPromise(promise);
      promise.then(
        function(response)  {
	  //console.log(response);
	  if(response.data.alerts.length)  {
	    $scope.collectionAlerts = response.data.alerts;
	    console.log("WARNING: Alerts found in response fetching collection " + $scope.collection.pid);	    
	  }
	  else {
	    if (response.data.hits) {
	      $scope.hits = response.data.hits;
  	      if ($scope.hits > $scope.maxCollectionSize) {
	        $scope.largeCollectionFlag = 1;
	      }
	    }
	    if (typeof response.data.objects !== 'undefined' && response.data.objects.length)  {
	      $scope.objects = processObjects(response.data.objects);             
	      // Get metadata of first object in collection
	      //console.log($scope.objects[0]);
       	      $scope.detail($scope.objects[0]); 
	    }
	    else {
              $scope.collectionError.status = -1;
	      console.log("ERROR "  + $scope.collectionError.status + ": Get collection objects " + $scope.collection.pid + " error");
	    }
	  }
        },
        function(response) {
          if(response.data) {
	    $scope.collectionError.status = response.status;
            console.log("ERROR "  + $scope.collectionError.status + ": Get collection objects " + $scope.collection.pid + " failed");                      
          }
          if (response.status === -1) {
            $scope.collectionError.status = -1;
            console.log("ERROR " + $scope.collectionError.status + ": Get collection objects " + $scope.collection.pid + " connection timeouted");
          }
	}
      );        
      return;
    }

    // Get item metadata
    function detail(obj) {
      if (typeof obj === 'undefined') {
	      console.log("Current object undefined");
        return;
      }      
      // Check if object is already the current (avoid useless API calls)
      if ($scope.objCurrent === obj)  {
        return;
      }
      else  {
        $scope.objCurrent = obj;
      }
      // Set top-image URL (480px Phaidra image preview)
      $scope.topImageURL = $scope.phaidraInstance + '/preview/' + $scope.objCurrent.pid;
      // Get MUSEO html if it exists
      var promiseMuseo = metadataMuseoService.getMetadata($scope.objCurrent.pid, $scope.phaidraInstance);
      promiseMuseo.then(
	function(response) {
       	  if(typeof response.data !== 'undefined' && response.data.length){
            //console.log(response);
            $scope.MUSEOmetadata = response.data;
	    $scope.hasMUSEO = 1;
          }
        },
        function(response) {
	   //manage MUSEO metadata error
	   console.log('Can\'t found MUSEO metadata');
	}
      ).then(
	function(){
	  //console.log($scope.hasMUSEO);
          var promise = metadataService.getMetadata($scope.objCurrent.pid, $scope.apiInstance);      
          //$scope.loadingMetadataTracker.addPromise(promise);
          promise.then(
	    function(response) {
	    if(response.data.metadata.alerts.length) {
	      $scope.itemMetadataAlerts = response.data.metadata.alerts;
	      console.log("WARNING: Alerts found in metadata response for object " + $scope.objCurrent.pid)
	    }
	    else {
	      if (typeof response.data.metadata.dc !== 'undefined' && response.data.metadata.dc.length)  {
	        $scope.itemMetadata = response.data.metadata.dc;
	        $scope.metadataObj = processMetadata($scope.itemMetadata);
	        // Metadata field as array (full metadata view)
	        $scope.metadataArr = [];
	        angular.forEach($scope.metadataObj, function(val, key) {
		  $scope.metadataArr.push({label:key, text:val.text, pos:val.pos});
	        });
	        if ($scope.metadataObj.type.text)  {
	    	  switch($scope.metadataObj.type.text) {
  		    case 'Book':
	  	      $scope.resourceURL = $scope.fedoraInstance + '/fedora/get/' + $scope.objCurrent.pid + '/bdef:Book/view';
	 	        break;	
		      case 'Collection':
	  	        $scope.resourceURL = $scope.phaidraInstance + '/detail_object/' + $scope.objCurrent.pid;
	  	        break;
		      default:
	 	        $scope.resourceURL = $scope.fedoraInstance + '/fedora/get/' + $scope.objCurrent.pid + '/bdef:Content/get';
		  }
       	        }
	    }
	    else {
	      $scope.itemMetadataError.status = -1;
	      console.log("ERROR " + $scope.objCurrent.pid + ": metadata response empty");
	    }
	  }
	},
	function(response) {
	  if(response.data) {
	    $scope.itemMetadataError.status = response.status;
	    console.log("ERROR "  + $scope.itemMetadataError.status + ": Get object " + $scope.objCurrent.pid + " metadata retrieval failed");                        	      
	  }
	  if (response.status === -1) {
	    $scope.itemMetadataError.status = -1;
	    console.log("ERROR " + $scope.itemMetadataError.status + ": Get object " + $scope.objCurrent.pid + " metadata connection timeouted");	      	}
	}
      ); // Get DC metadata promise
      });
      return;
    }

    function getCollectionDetail(pid) {
      var promise = metadataService.getMetadata(pid, $scope.apiInstance);
//      $scope.loadingCollectionMetadataTracker.addPromise(promise);
      promise.then(
        function(response) {
          if(response.data.metadata.alerts.length) {
            $scope.collectionMetadataAlerts = response.data.metadata.alerts;
            console.log("WARNING: Alerts found in metadata response for object " + pid)
          }
          else {
	    if (typeof response.data.metadata.dc !== 'undefined' && response.data.metadata.dc.length) {
	      $scope.collectionMetadata = response.data.metadata.dc;
              $scope.collectionMetadataObj = processMetadata($scope.collectionMetadata);
	      // Title is the only field we are interested in collection object
	      if ($scope.collectionMetadataObj.title.text) {
  	        $scope.collectionTitle = $scope.collectionMetadataObj.title.text;
	      }
	      else {
		$scope.collectionMetadataError.status = -1;
		console.log("ERROR " + pid + ": metadata processing error");
	      }
	    }
	    else {
              $scope.collectionMetadataError.status = -1;
   	      console.log("ERROR " + pid + ": metadata response empty");
	    }
	  }	
        },
	function(response) {
          if(response.data) {
            $scope.collectionMetadataError.status = response.status;
            console.log("ERROR "  + $scope.itemMetadataError.status + ": Get object " + pid + " metadata retrieval failed");         
          }
          if (response.status === -1) {
            $scope.collectionMetadataError.status = -1;
            console.log("ERROR " + $scope.itemMetadataError.status + ": Get object " + pid + " metadata connection timeouted");      
          }
        }
      );
      return;
    }

    }]) // END CONTROLLER

  // Return app instance for requirejs
  return app;
});
})();